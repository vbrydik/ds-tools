from dataclasses import dataclass

import scipy
import scipy.fftpack
import scipy.io.wavfile as wavfile
import numpy as np
from matplotlib import pyplot as plt


def _open_wav(filename):
    # Open wav file using scipy
    sample_rate, signal = wavfile.read(filename)
    return signal, sample_rate


@dataclass
class FFTResult:
    """
    TODO: Add description
    """
    n: int
    secs: float
    ts: float
    t: np.ndarray
    fft: np.ndarray
    fft_side: np.ndarray


def compute_fft(signal, sample_rate) -> FFTResult:
    """
    TODO: Add description
    """

    # Convert signal to mono, if stereo
    if len(signal.shape) == 2:
        signal = signal.sum(axis=1) / 2

    # Compute parameters
    N = signal.shape[0]             # Number of samples
    secs = N / float(sample_rate)   # Length in seconds
    Ts = 1.0 / sample_rate          # Time steps 

    # Compute FFT
    t = scipy.arange(0, secs, Ts)   # time vector as scipy arange field / numpy.ndarray
    FFT = abs(scipy.fft.fft(signal))
    FFT_side = FFT[range(N//2)]     # one side FFT range

    # Return results
    return FFTResult(
        n=N,
        secs=secs,
        ts=Ts,
        t=t,
        fft=FFT,
        fft_side=FFT_side,
    )


def visualize_fft(
        signal, 
        fft_result: FFTResult,
        vis_n_fft: int = None,
    ):
    """
    TODO: Add description
    """

    # Convert signal to mono, if stereo
    if len(signal.shape) == 2:
        signal = signal.sum(axis=1) / 2
      
    # Visualize FFT results
    freqs = scipy.fftpack.fftfreq(signal.size, fft_result.t[1]-fft_result.t[0])
    fft_freqs = np.array(freqs)
    freqs_side = freqs[range(fft_result.n//2)] # one side frequency range
    fft_freqs_side = np.array(freqs_side)
    plt.subplot(211)
    p1 = plt.plot(fft_result.t, signal, "g") # plotting the signal
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    # plt.subplot(212)
    # p2 = plt.plot(freqs, fft_result.fft, "r") # plotting the complete fft spectrum
    # plt.xlabel('Frequency (Hz)')
    # plt.ylabel('Count dbl-sided')

    fft_side = fft_result.fft_side
    if vis_n_fft is not None: 
        freqs_side = freqs_side[:vis_n_fft]
        fft_side = fft_side[:vis_n_fft]
        
    plt.subplot(212)
    p3 = plt.plot(freqs_side, abs(scipy.signal.savgol_filter(fft_side, 50, 3)), "b") # plotting the positive fft spectrum
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Count single-sided')
    plt.show()


if __name__ == "__main__":

    default_audio = "/home/vb/data/reference_audio_vitalik/reference_voice_vitalik.24k.wav"

    signal, sample_rate = _open_wav(default_audio)
    fft_result = compute_fft(signal, sample_rate)

    visualize_fft(signal, fft_result, vis_n_fft=64000)
    




