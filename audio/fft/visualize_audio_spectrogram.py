import librosa
import numpy as np
import soundfile as sf
import matplotlib.pyplot as plt
import scipy.signal

from scipy.ndimage.filters import gaussian_filter


def _conv_sr(data, sr, new_sr):
    if sr == new_sr:
        return data
    n_samples = round(len(data) * new_sr / sr)
    return scipy.signal.resample(data, n_samples)


# # read file
# file    = "/home/vb/data/reference_audio_vitalik/reference_voice_vitalik.24k.wav"
# file    = "./input_cv_male_1.mp3"
# file    = "./test_write.wav"
# sig, fs = sf.read(file)
# sig     = _conv_sr(sig, fs, 24000)
# 
# # process
# # abs_spectrogram = np.abs(librosa.core.spectrum.stft(sig))
# abs_spectrogram = np.abs(librosa.stft(sig))
# 
# mel_spec = librosa.amplitude_to_db(abs_spectrogram, ref=np.max)
# 
# # mel_spec[:20, :] = -80
# # mel_spec[-900:, :] = -80
# # mel_spec[mel_spec < -60] = mel_spec.min()
# # mel_spec = gaussian_filter(mel_spec, sigma=(3, 1))
# 
# # mel_spec += 40
# 
# Visualize
# print(mel_spec.shape, mel_spec.max(), mel_spec.min())
# librosa.display.specshow(mel_spec, sr=fs, y_axis='log', x_axis='time')
# plt.colorbar(format='%+2.0f dB')
# plt.show()
# 
# 
# 
# audio_signal = librosa.core.spectrum.griffinlim(
#     librosa.db_to_amplitude(mel_spec)
# )
# 
# print(audio_signal, audio_signal.shape)
# 
# # write output
# sf.write('test2.wav', audio_signal, fs)
# # librosa.write_wav('test2.wav', audio_signal, fs)



# Correction
file_orig     = "./input_cv_male_1.mp3"
file_synth    = "./test_write.wav"
# file_synth    = "input_synth_cv1.wav"
target_sr     = 24000
sig_orig, fs  = sf.read(file_orig)
sig_orig      = _conv_sr(sig_orig, fs, target_sr)
sig_synth, fs = sf.read(file_synth)
sig_synth     = _conv_sr(sig_synth, fs, target_sr)

spec_orig  = np.abs(librosa.stft(sig_orig))
spec_synth = np.abs(librosa.stft(sig_synth))

print(spec_orig.mean(),  spec_orig.std())
print(spec_synth.mean(), spec_synth.std())

orig_mean, orig_std = spec_orig.mean(), spec_orig.std()
synth_mean, synth_std = spec_synth.mean(), spec_synth.std()

spec_orig_corr = spec_orig.copy()
spec_orig_corr = (spec_orig_corr - orig_mean) * np.sqrt(orig_std / synth_std) + synth_mean

print(spec_orig_corr.mean(),  spec_orig_corr.std())

# mel_spec = librosa.amplitude_to_db(spec_orig_corr, ref=np.max)
mel_spec = librosa.amplitude_to_db(spec_synth, ref=np.max)

_min = mel_spec.min()
mel_spec[:22, :] = _min
mel_spec[-850:, :] = _min
mel_spec[mel_spec < -70] = _min
mel_spec[mel_spec > -10] = -10 # mel_spec.min()
# mel_spec = gaussian_filter(mel_spec, sigma=(2, 1))
mel_spec += 40

# Visualize
print(mel_spec.shape, mel_spec.max(), mel_spec.min())
librosa.display.specshow(mel_spec, sr=fs, y_axis='log', x_axis='time', cmap='inferno')
plt.colorbar(format='%+2.0f dB')
plt.show()

audio_signal = librosa.core.spectrum.griffinlim(
    librosa.db_to_amplitude(mel_spec), n_iter=32
)

print(audio_signal, audio_signal.shape)

# write output
# sf.write('test2.wav', audio_signal, fs)
sf.write('test2_synth.wav', audio_signal, fs)




